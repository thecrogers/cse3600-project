//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication to run the election process. 
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/signal.h>
#define STD_IN 0
#define STD_OUT 1
#define BUFFERSIZE 7


void usr1Handler()
{
char temp[6]="Se?rd0";
fprintf(stderr,"signal 1 handeled. pid: %d\n",getpid());
write(STD_OUT,temp,BUFFERSIZE);
}
void usr2Handler()
{
char temp[6]="se?rD0";
fprintf(stderr,"signal 2 handeled pid: %d.\n",getpid());
write(STD_OUT,temp,BUFFERSIZE);
}

int main(int argc, char* argv[])
{
        char myID;
        int numberOfProcess,i,val,offset;
        pid_t childpid;
        pid_t mypid;
        pid_t currentid;
        int FD[2];   //Reusable Pipe
        char bufferIN[BUFFERSIZE] = "";
        char bufferOUT[BUFFERSIZE] ="se?rd0";
        mypid = getpid();
        myID = 'A';
        
        if(argc != 2)
        {
                //Check for proper number of command line arguments
                fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
                return 1;
        }


        //Establish Original Pipe
        pipe(FD);
        if( dup2(FD[0], STD_IN) <0)
        {
                fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

        }
        //set stdout to pipe write alpha
        if( dup2(FD[1], STD_OUT) < 0 )
        {
                fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
        }

        fprintf(stderr,"PID: %d PNUM: %c started\n",getpid(),myID);
        numberOfProcess = atoi(argv[1]);
       
       for(i = 1; i < numberOfProcess; i++)
        {
                childpid = fork();
                if(childpid==-1)
                {
                        fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
                }

                if(childpid!=0){break;}

        }


        currentid=getpid();
        if(currentid!=mypid)//does not allow alpha to have overriden std_in
        {
          
                if( dup2(FD[0], STD_IN) < 0)
                {
                        fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
                }
              
               offset=currentid-mypid;
               myID+=offset;

               fprintf(stderr,"PID: %d PNum: %c started\n",getpid(),myID);


               signal(SIGUSR1, usr1Handler);
               signal(SIGUSR2, usr2Handler);
               while(bufferIN[4]!='D'&&bufferIN[5]!='2')//loops unil election process is over.
                 {
                  sleep(1);
                  read(STD_IN,bufferIN,BUFFERSIZE);
                
                 if(bufferIN[0]=='S'&&bufferIN[1]=='E')
                    {
                      //val=strcmp(myID,bufferIN[2]);
                      if(myID==bufferIN[2])
                      {
                        val = bufferIN[5]-'0';
                        val+=1;
                        bufferIN[5]=(char)(((int)'0')+val);
                      }
                      else if(myID>bufferIN[2])
                      {
                       fprintf(stderr,"%c is the new leader\n",myID);
                       bufferIN[2]=myID;
                      }
                      else if(bufferIN[2]!=myID&&bufferIN[5]=='1')
                      {
                        fprintf(stderr,"%c bows down to %c as the new leader\n",myID,bufferIN[2]);
                      }
                    }
                    else if(bufferIN[0]=='S'&&bufferIN[1]!='E')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c\n",myID);
                    fprintf(stderr,"%c is the new leader\n",myID);
                       bufferIN[2]=myID;
                    bufferIN[1]='E';
                   }
                   // fprintf(stderr,"value of count in char:%c, in int: %d\n",bufferIN[5],val);
                    
                  strcpy(bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
                 
               while(bufferIN[4]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }

                fprintf(stderr,"deconstucting pid: %d\n",getpid());

        }


        if( currentid==mypid)//parent code.
        {
                sleep(1);

                if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
                {
                        fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
                }
               signal(SIGUSR1, usr1Handler);
               signal(SIGUSR2, usr2Handler);
              
               while(bufferIN[4]!='D'&&bufferIN[5]!='2')//loops unil election process is over.
                 {
                  sleep(1);
                  read(STD_IN,bufferIN,BUFFERSIZE);
                
                 if(bufferIN[0]=='S'&&bufferIN[1]=='E')
                    {
                      //val=strcmp(myID,bufferIN[2]);
                      if(myID==bufferIN[2])
                      {
                        val = bufferIN[5]-'0';
                        val+=1;
                        bufferIN[5]=(char)(((int)'0')+val);
                      }
                      else if(myID>bufferIN[2])
                      {
                       fprintf(stderr,"%c is the new leader\n",myID);
                       bufferIN[2]=myID;
                      }
                      else if(bufferIN[2]!=myID&&bufferIN[5]=='1')
                      {
                        fprintf(stderr,"%c bows down to %c as the new leader\n",myID,bufferIN[2]);
                      }
                    }
                    else if(bufferIN[0]=='S'&&bufferIN[1]!='E')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c pid: %d\n",myID,getpid());
                    fprintf(stderr,"%c is the new leader\n",myID);
                       bufferIN[2]=myID;
                    bufferIN[1]='E';
                   }
                   // fprintf(stderr,"value of count in char:%c, in int: %d\n",bufferIN[5],val);
                    
                  strcpy(bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
                 
               while(bufferIN[4]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }
                sleep(1);
                wait(NULL);
                fprintf(stderr,"deconstucting pid: %d\n",getpid());
        }

        //Close created pipe
        close(FD[0]);
        close(FD[1]);
        return 0;
}
