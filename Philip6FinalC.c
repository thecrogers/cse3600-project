//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/signal.h>

#define STD_IN 0
#define STD_OUT 1

bool start = false;
bool stop =  false;
bool safe = false;
bool Safe = false;

struct election{
	pid_t highestpid;
	int offset;
	char highestID;
	bool start;
	bool stop;
        bool safe;
        bool Safe;

};


void usr1Handler()
{
        safe = true;
	start = true;
	fprintf(stderr,"Boom baby!\n");

}
void usr2Handler()
{
	fprintf(stderr,"Come on you apes, you wanna live forever?!\n");
	stop = true;
        Safe=true;
}

int main(int argc, char* argv[])
{
	//signal(SIGUSR1, SIG_IGN); //Ignore until it's time.
	//signal(SIGUSR2, SIG_IGN); //Ignore until it's time.
	int numberOfProcess,i;
	char myID;
	pid_t childpid;
	pid_t mypid;
	pid_t currentid;
	int FD[2];   //Reusable Pipe
	struct election elect;
	struct election *plect;
	plect = &elect;

	//Init Struct Here
	elect.highestpid = mypid;
	elect.offset = 0;
	elect.highestID = ' ';
	elect.start = start;
	elect.stop = stop;
        elect.safe = safe;
        elect.Safe= Safe;

	//Init some stuff pre-fork
	mypid = getpid();
	myID = 'A';

	if(argc != 2)
	{
		//Check for proper number of command line arguments
		fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
		return 1;
	}



	//Establish Original Pipe
	pipe(FD);
	if( dup2(FD[0], STD_OUT) <0)
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

	}
	//set stdout to pipe write alpha
	if( dup2(FD[1], STD_OUT) < 0 )
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
	}

	//Throwing a fork real quick.
	fprintf(stderr,"PID: %d PNum: %c started\n",getpid(),myID);
	elect.offset++;
	write(STD_OUT,plect,sizeof(*plect));


	//Create processes
	numberOfProcess = atoi(argv[1]);

	for(i = 1; i < numberOfProcess; i++)
	{
		childpid = fork();
		if(childpid==-1)
		{
			fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
		}

		if(childpid!=0){
			break;
		}

	}


	currentid=getpid();


	if(currentid!=mypid)//does not allow alpha to have overriden std_in
	{
		if( dup2(FD[0], STD_IN) < 0)
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}

		read(STD_IN, plect, sizeof(*plect));

                myID+=elect.offset;
		elect.offset++;
                fprintf(stderr,"PID: %d PNum: %c started\n",getpid(),myID);

		write(STD_OUT,plect,sizeof(*plect));
		signal(SIGUSR1, usr1Handler);
                signal(SIGUSR2, usr2Handler);
		while(plect->stop==false&&plect->start==false) //The most inefficient while loop ever created! MUHAHAHAHA.
		{
                  elect.start=start;
                  elect.stop=stop;
		  sleep(1);
                  if(plect->safe==true)
                  {
                   fprintf(stderr,"pid %d is in safe\n",getpid());
                   read(STD_IN,plect,sizeof(*plect));
                  }

			if(plect->start == true)
			{
                         elect.safe=safe;
			 elect.start = start;
		  	 write(STD_OUT,plect,sizeof(*plect));
			}


		}
            if(plect->start==true)
             {
		//Manipulation of struct here
		if(getpid()>getppid())
		{
			fprintf(stderr,"%c is your new leader, bow down!\n",myID);
                }
		else //If I don't win, suck up to your master.
		{
			fprintf(stderr,"%c bows down to new leader, %c\n",myID,plect->highestID);
		}
             }
                 //Demolition the ring handler.
		do {
                        elect.stop=stop;
      			sleep(1);
                        if(plect->Safe==true)
                        {
                         fprintf(stderr,"process is in the safe area: %d\n",getpid());
			 read(STD_IN, plect, sizeof(*plect));
			}

                        if(plect->stop == true)
			{
                                elect.Safe=Safe;
				write(STD_OUT,plect,sizeof(*plect));
				kill(getpid(), SIGTERM);
			}
		}while(plect->stop == false); //The most inefficient while loop ever created! MUHAHAHAHA.

		//Method of shutting down all the processes
	}

	if( currentid==mypid) //parent code.
	{
	sleep(1);
		if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}
	     signal(SIGUSR2, usr2Handler);
             signal(SIGUSR1, usr1Handler);

		while(plect->start == false&&plect->stop ==false) //The most inefficient while loop ever created! MUHAHAHAHA.
		{
                      elect.start=start;
                      elect.stop = stop;
                      if(plect->safe==true)
                        {
                       fprintf(stderr,"in parent safe\n");
                       read(STD_IN, plect, sizeof(*plect));
                       }
                       if(plect->start == true)
			{
				elect.safe = safe;
				write(STD_OUT,plect,sizeof(*plect));
			}
		}
               if(plect->start==true)
                {
		//Manipulation of struct here
		if(plect->highestpid<= getpid()) //If I won the election
		{
			elect.highestpid = getpid();
			elect.highestID = myID;

			fprintf(stderr,"%c is your new leader, bow down!\n",myID);
		}
		else //If I don't win, suck up your master.
		{
			fprintf(stderr,"%c bows down to new leader, %c\n",myID,plect->highestID);
		}
               }
		//Shut it all down.
                do
		{

                        elect.stop=stop;
			sleep(1);
                        if(plect->Safe==true);
                        {
			read(STD_IN, plect, sizeof(*plect));
		 	}
                       if(plect->stop == true)
			{
				elect.Safe = Safe;
				write(STD_OUT,plect,sizeof(*plect));
			}
		}while(plect->stop == false); //The most inefficient while loop ever created! MUHAHAHAHA.

		wait(NULL);
	}



	//Close created pipe
	close(FD[0]);
	close(FD[1]);
	return 0;
}


