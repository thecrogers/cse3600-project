//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication and fib sequence
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <gmp.h>
#define STD_IN 0
#define STD_OUT 1
#define BUFFERSIZE 20

//Long long unsigned doesn't go above the 94th. Just a heads up. We'd need to use or implement a bigNumber library
//Rather just work on part C for now.
struct fibseq{
  mpz_t    x;
  mpz_t    y;
  mpz_t    z;
  int count ;
   };


	struct fibseq fib;
int main(int argc, char* argv[])
{



	int numberOfProcess,i,tmp,count,Temp;
	pid_t childpid;
	pid_t mypid;
	pid_t currentid;
	int FD[2];   //Reusable Pipe
	mpz_init(fib.x);
        mpz_init(fib.y);
        mpz_init(fib.z);
        int temp1,temp2;
        int temp3=0;
        int temp4=1;
	struct fibseq *pfib;
	pfib = &fib;
	mypid = getpid();
        fib.count=1;
	//fprintf(stderr," alpha pid: %d\n",mypid);
	if(argc != 4)
	{
		//Check for proper number of command line arguments
		fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
		return 1;
	}

	//Init struct.
	numberOfProcess = atoi(argv[1]);
	temp1=atoi(argv[2]);
	temp2=atoi(argv[3]);
	mpz_set_d(fib.x,temp1);
 	mpz_set_d(fib.y,temp2);
 	mpz_set_d(fib.z,temp3);
        Temp=0;
        currentid=getpid();
        if(fib.x==0&&fib.y==0)
         {
           fprintf(stderr,"PID: %d PNum:%d ",currentid, fib.count);
           gmp_fprintf(stderr,"%Zd + %Zd = %Zd \n",fib.x,fib.y, fib.z);
           mpz_set_d(fib.x,temp4);
           fib.count++;

         }
	//Establish Original Pipe
	pipe(FD);
	if( dup2(FD[0], STD_OUT) <0)
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

	}
	//set stdout to pipe write alpha
	if( dup2(FD[1], STD_OUT) < 0 )
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
	}

	write(STD_OUT,pfib,sizeof(*pfib));


	for(i = Temp; i < numberOfProcess; i++)
	{
		childpid = fork();
		if(childpid==-1)
		{
			fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
		}

		if(childpid!=0){break;}

	}

	currentid=getpid();
	if(currentid!=mypid)//does not allow alpha to have overriden std_in
	{
		if( dup2(FD[0], STD_IN) < 0)
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}

		read(STD_IN, pfib, sizeof(*pfib));
		//Do the math here for the fib does not print correct first number.
                mpz_add(fib.z, fib.x , fib.y);
                fprintf(stderr,"PID: %d PNum:%d ",currentid, fib.count);
                 gmp_fprintf(stderr,"%Zd + %Zd = %Zd \n",fib.x,fib.y, fib.z);
                 fib.count++;
                 mpz_set(fib.x,fib.y);
                 mpz_set(fib.y,fib.z);
		write(STD_OUT,pfib, sizeof(*pfib));

	}


	if( currentid==mypid)//parent code.
	{
		sleep(1);

		if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}
		read(STD_IN, pfib,sizeof(*pfib));
	//	fprintf(stderr, "This is alphaParent process %d BufferIN: %s & BufferOut: %s\n",getpid(),bufferIN, bufferOUT);


		wait(NULL);
		fprintf(stderr, "\n***Child finished***\n");
	}
	//Close created pipe
	close(FD[0]);
	close(FD[1]);
	return 0;
}
