//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#define STD_IN 0
#define STD_OUT 1
#define BUFFERSIZE 20
int main(int argc, char* argv[])
{
	int numberOfProcess,i;
	pid_t childpid;
	pid_t mypid;
	pid_t currentid;
	int FD[2];   //Reusable Pipe
	char bufferIN[BUFFERSIZE] = "";
	char bufferOUT[BUFFERSIZE] ="Marco";

	mypid = getpid();
	fprintf(stderr," alpha pid: %d\n",mypid);
	if(argc != 2)
	{
		//Check for proper number of command line arguments
		fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
		return 1;
	}


	//Establish Original Pipe
	pipe(FD);
	if( dup2(FD[0], STD_OUT) <0)
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

	}
	//set stdout to pipe write alpha
	if( dup2(FD[1], STD_OUT) < 0 )
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
	}

	write(STD_OUT,bufferOUT,strlen(bufferOUT)+1);
	numberOfProcess = atoi(argv[1]);

	for(i = 1; i < numberOfProcess; i++)
	{
		childpid = fork();
		if(childpid==-1)
		{
			fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
		}

		if(childpid!=0){break;}

	}


	currentid=getpid();
	if(currentid!=mypid)//does not allow alpha to have overriden std_in
	{
		if( dup2(FD[0], STD_IN) < 0)
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}

		read(STD_IN, bufferIN, BUFFERSIZE);
		strcpy(bufferOUT,bufferIN);
		strcat(bufferOUT,"+");
		currentid=getpid();
		fprintf(stderr, "This is process %d reading %s & writing: %s\n",currentid,bufferIN, bufferOUT);
		write(STD_OUT,bufferOUT,strlen(bufferOUT)+1);

	}


	if( currentid==mypid)//parent code.
	{
		sleep(1);

		if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}
		read(STD_IN, bufferIN,BUFFERSIZE);
		fprintf(stderr, "This is alphaParent process %d BufferIN: %s & BufferOut: %s\n",getpid(),bufferIN, bufferOUT);


		wait(NULL);
		fprintf(stderr, "\n***Child finished***\n");
	}

	//Close created pipe
	close(FD[0]);
	close(FD[1]);
	return 0;
}
