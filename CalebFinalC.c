//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication to run the election process. 
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/signal.h>
#define STD_IN 0
#define STD_OUT 1
#define BUFFERSIZE 6


enum {START,RUNNING,LEADER,END,DECONSTRUCT};

struct election
{
int offset;
};

void usr1Handler()
{
char temp[6]="Se?rd";
fprintf(stderr,"signal 1 handeled.\n");
write(STD_OUT,temp,BUFFERSIZE);
}
void usr2Handler()
{
char temp[6]="se?rD";
fprintf(stderr,"signal 2 handeled.\n");
write(STD_OUT,temp,BUFFERSIZE);
}

int main(int argc, char* argv[])
{
        char myID,localID;
        int numberOfProcess,i,val;
        pid_t childpid;
        pid_t mypid;
        pid_t currentid;
        struct election elect;
        struct election *plect;
        int FD[2];   //Reusable Pipe
        char bufferIN[BUFFERSIZE] = "";
        char bufferOUT[BUFFERSIZE] ="se?rd";
        elect.offset = 0;
        mypid = getpid();
        myID = 'A';
        plect = &elect;
        if(argc != 2)
        {
                //Check for proper number of command line arguments
                fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
                return 1;
        }


        //Establish Original Pipe
        pipe(FD);
        if( dup2(FD[0], STD_IN) <0)
        {
                fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

        }
        //set stdout to pipe write alpha
        if( dup2(FD[1], STD_OUT) < 0 )
        {
                fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
        }

        fprintf(stderr,"PID: %d PNUM: %c started\n",getpid(),myID);
        localID=myID;
        elect.offset++;
        numberOfProcess = atoi(argv[1]);
        write(STD_OUT,plect,sizeof(*plect));

        for(i = 1; i < numberOfProcess; i++)
        {
                childpid = fork();
                if(childpid==-1)
                {
                        fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
                }

                if(childpid!=0){break;}

        }


        currentid=getpid();
        if(currentid!=mypid)//does not allow alpha to have overriden std_in
        {
                if( dup2(FD[0], STD_IN) < 0)
                {
                        fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
                }
               read(STD_IN,plect,sizeof(*plect));

               myID=myID+elect.offset;
               localID=myID;
               elect.offset++;

               fprintf(stderr,"PID: %d PNum: %c started\n",getpid(),myID);

               write(STD_OUT,plect,sizeof(*plect));

               sleep(1);

               signal(SIGUSR1, usr1Handler);
               signal(SIGUSR2, usr2Handler);
               while(bufferIN[END]!='R'&&bufferIN[DECONSTRUCT]!='D')//loops unil election process is over.
                 {
                  sleep(1);
                  read(STD_IN,bufferIN,BUFFERSIZE);
                  if(bufferIN[START]=='S'&&bufferIN[RUNNING]!='E')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c\n",localID);

                    bufferIN[RUNNING]='E';
                   }
                   else if(bufferIN[START]=='S'&&bufferIN[RUNNING]=='E')
                    {
                      //val=strcmp(myID,bufferIN[LEADER]);
                      if(myID>bufferIN[LEADER])
                      {
                       fprintf(stderr,"%c is the new leader\n",localID);
                       bufferIN[LEADER]=localID;
                      }
                      else if(bufferIN[LEADER]!=localID)
                      {
                        fprintf(stderr,"%c bows down to %c as the new leader\n",localID,bufferIN[LEADER]);
                      }
                    }
                    
                  strcpy(bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
               while(bufferIN[DECONSTRUCT]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }

                fprintf(stderr,"deconstucting pid: %d\n",getpid());

        }


        if( currentid==mypid)//parent code.
        {
                sleep(1);

                if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
                {
                        fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
                }
               signal(SIGUSR1, usr1Handler);
               signal(SIGUSR2, usr2Handler);
               while(bufferIN[END]!='R'&&bufferIN[DECONSTRUCT]!='D')//loops unil election process is over.
                 {
                  sleep(1);
                  read(STD_IN,bufferIN,BUFFERSIZE);
                  if(bufferIN[START]=='S'&&bufferIN[RUNNING]!='E')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c\n",localID);
                    
                    bufferIN[RUNNING]='E';
                   }
                   else if(bufferIN[START]=='S'&&bufferIN[RUNNING]=='E')
                    {
                      //val=strcmp(myID,bufferIN[LEADER]);
                      if(myID>bufferIN[LEADER])
                      {
                       fprintf(stderr,"%c is the new leader\n",localID);
                       bufferIN[LEADER]=myID;
                      }
                      else if(bufferIN[END]=='R'&&bufferIN[LEADER]!=localID)
                      {
                        fprintf(stderr,"%c bows down to %c as the new leader\n",localID,bufferIN[LEADER]);
                      }
                    }
                    
                  strcpy( bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
               while(bufferIN[DECONSTRUCT]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }
                wait(NULL);
                fprintf(stderr,"deconstructing pid: %d\n",getpid());
        }

        //Close created pipe
        close(FD[0]);
        close(FD[1]);
        return 0;
}
