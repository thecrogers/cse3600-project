//Programmer: Caleb Rogers, Philip Becker
//Course:  CSCE 36000
//Description:  Establishes process ring with piped communication to run the election process. 
//File:   TokenRingExample.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/signal.h>
#define STD_IN 0
#define STD_OUT 1
#define BUFFERSIZE 6

struct election
{
int offset;
char HID;
};

void usr1Handler()
{
char temp[6]="Secrd";
fprintf(stderr,"signal 1 handeled.\n");
write(STD_OUT,temp,BUFFERSIZE);
}
void usr2Handler()
{
char temp[6]="secrD";
fprintf(stderr,"signal 2 handeled.\n");
write(STD_OUT,temp,BUFFERSIZE);
}

int main(int argc, char* argv[])
{
        char myID;
	int numberOfProcess,i;
	pid_t childpid;
	pid_t mypid;
	pid_t currentid;
        struct election elect;
        struct election *plect;
	int FD[2];   //Reusable Pipe
	char bufferIN[BUFFERSIZE] = "";
	char bufferOUT[BUFFERSIZE] ="secrd";
        elect.offset = 0;
        elect.HID='A';
	mypid = getpid();
        myID = 'A';
        char temp[BUFFERSIZE]="secrd";
        plect = &elect;
	if(argc != 2)
	{
		//Check for proper number of command line arguments
		fprintf(stderr, "Usage: %s number of processes\n", argv[0]);
		return 1;
	}


	//Establish Original Pipe
	pipe(FD);
	if( dup2(FD[0], STD_IN) <0)
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );

	}
	//set stdout to pipe write alpha
	if( dup2(FD[1], STD_OUT) < 0 )
	{
		fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
	}
        fprintf(stderr,"Send user signal 1 to start the election.\n");
        fprintf(stderr,"Send user signal 2 to disband the tokenring\n\n");
	fprintf(stderr,"PID: %d PNUM: %c started\n",getpid(),myID);
        elect.offset++;
	numberOfProcess = atoi(argv[1]);
        write(STD_OUT,plect,sizeof(*plect));

	for(i = 1; i < numberOfProcess; i++)
	{
		childpid = fork();
		if(childpid==-1)
		{
			fprintf(stderr, "Fork Error - Error code: %s\n", strerror( errno ) );
		}

		if(childpid!=0){break;}

	}


	currentid=getpid();
	if(currentid!=mypid)//does not allow alpha to have overriden std_in
	{
		if( dup2(FD[0], STD_IN) < 0)
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
		}
               read(STD_IN,plect,sizeof(*plect));

               myID+=elect.offset;
               elect.offset++;
               fprintf(stderr,"PID: %d PNum: %c started\n",getpid(),myID);
               if(getpid()>getppid()){elect.HID=myID;}
               write(STD_OUT,plect,sizeof(*plect));
               sleep(1);
               signal(SIGUSR1, usr1Handler);
               signal(SIGUSR2, usr2Handler);
               while(bufferIN[3]!='R'&&bufferIN[4]!='D')//loops unil election process is over.
                 {
                  read(STD_IN,bufferIN,BUFFERSIZE);
                  if(bufferIN[0]=='S')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c",myID);
                    fprintf(stderr,"%c has entered the election",myID);
                    fprintf(stderr,"%c is the new leader\n",myID);
                    temp[BUFFERSIZE]="sEcrd";
                    temp[2]=myID;
                    sleep(1);
                   }
                 else if(bufferIN[1]='E')//election occuring
                   {
                   if(strcmp(myID,bufferIN[2])>0)
                    {
                     bufferIN[2]=myID;
                     fprintf(stderr,"%c is now the new leader\n");
                    }
                   else
                    {
                     fprintf(stderr,"%c bows down to the new leader %c\n",myID,bufferIN[2]);
                    }
                   }
                  else if(bufferIN[3]=='R')
                    {
                     fprintf(stderr,"eletion complete %c is the new leader\n",bufferIN[2]);
                    }
                  strcpy(bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
               while(bufferIN[4]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }

                fprintf(stderr,"deconstucting pid: %d\n",getpid());

	}


	if( currentid==mypid)//parent code.
	{
		sleep(1);

		if(dup2(FD[0], STD_IN) < 0) //connects the alpha parent to last child
		{
			fprintf(stderr, "Pipe Error - Error code: %s\n", strerror( errno ) );
	        }
     while(bufferIN[3]!='R'&&bufferIN[4]!='D')//loops unil election process is over.
                 {
                  read(STD_IN,bufferIN,BUFFERSIZE);
                  if(bufferIN[0]=='S')//election has started.
                   {
                    //election process
                    fprintf(stderr,"election has started by: %c",myID);
                    fprintf(stderr,"%c has entered the election",myID);
                    fprintf(stderr,"%c is the new leader\n",myID);
                    temp[BUFFERSIZE]="sEcrd";
                    temp[2]=myID;
                    sleep(1);
                   }
                 else if(bufferIN[1]='E')//election occuring
                   {
                   if(strcmp(myID,bufferIN[2])>0)
                    {
                     bufferIN[2]=myID;
                     fprintf(stderr,"%c is now the new leader\n");
                    }
                   else
                    {
                     fprintf(stderr,"%c bows down to the new leader %c\n",myID,bufferIN[2]);
                    }
                   }
                  else if(bufferIN[3]=='R')
                    {
                     fprintf(stderr,"eletion complete %c is the new leader\n",bufferIN[2]);
                    }
                  strcpy(bufferOUT,bufferIN);
                  write(STD_OUT,bufferOUT,BUFFERSIZE);
                 }
               while(bufferIN[4]!='D')
                {
                 read(STD_IN,bufferIN,BUFFERSIZE);
                 strcpy(bufferOUT,bufferIN);
                 write(STD_OUT,bufferOUT,BUFFERSIZE);
                }
 		wait(NULL);
    fprintf(stderr,"deconstructing pid:%d\n",getpid());
	}

	//Close created pipe
	close(FD[0]);
	close(FD[1]);
	return 0;
}
